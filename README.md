# Rock Pi 4
The current recipe builds a Debian image based on testing

## Build
```
debos debimage-rockpi4.yml
```

## Flash
In order to flash the image on the eMMC the options are:
- MMC or eMMC adapter
- USB Flash

### MMC or eMMC adapter
With MMC or eMMC adapter the flashing process is trivial

```
dd if=debian-rockpi4.img of=/dev/mmcblk0
```

### USB Flash
In case of using USB Flash, follow the steps [here](https://wiki.radxa.com/Rockpi4/dev/usb-install) to 
- Install rkdeveloptool
- Boot the board into mask mode

```
rkdeveloptool wl 0 debian-rockpi4.img
```

## Console
The conection for the console is shown in the picture
![Console](img/console.jpg)

```
baudrate: 1500000
data bit: 8
stop bit: 1
parity  : none
flow control: none
```

## Power supply
There are several ways to feed energy to the Rock Pi 4

### USB type C
The easiest way is to connect a USB type C connector to the Rock Pi 4.

![Power_usb](img/power_usb.jpg)

### GPIO Header
The GPIO Header has pins 2 and 4 to used for connecting VCC 5 V. In latest revision of the board those pins are indicated with red.

https://wiki.radxa.com/Rockpi4/hardware/gpio

![Power_gpio_header](img/power_gpio_header.jpg)

In this case, probably it will be easier to use a different pin for console ground, like in the picture below

![Power_gpio_header_console](img/power_gpio_header_console.jpg)

## Network boot
In order to be able to test different kernels and rootfs network boot can be easily enabled in U-Boot by setting the default `bootcmd` to either `bootcmd_dhcp` or `bootcmd_pxe`. In order for this to work, Rock Pi 4 should be connected to a LAN which provides DHCP, TFTP and NFS server.

The following example shows TFTP configuration to enable PXE boot.

The files are placed inside /srv/tftp

```
$ ls -1 /srv/tftp/
initrd.img-5.4.0-4-arm64
pxelinux.cfg
rk3399-rock-pi-4.dtb
vmlinuz-5.4.0-4-arm64
```

And PXE configuration

```
$ cat /srv/tftp/pxelinux.cfg/default-arm-rk3399-evb_rk3399 
default l0
menu title U-Boot menu
prompt 0
timeout 50

label l0
        menu label Rock Pi 4 (Debian/Testing)
        linux vmlinuz-5.4.0-4-arm64
        fdt rk3399-rock-pi-4.dtb
	initrd initrd.img-5.4.0-4-arm64
        append loglevel=8 root=/dev/nfs ip=dhcp rootwait rw earlyprintk nfsroot=192.168.2.100:/srv/nfs/debian-arm64
```
